import pandas as pd
import numpy as np
from sklearn.neural_network import MLPRegressor
from sklearn.datasets import make_regression
from sklearn.model_selection import train_test_split

df = pd.read_csv('./data/GS66516T_final.csv', sep=';')
print(df)