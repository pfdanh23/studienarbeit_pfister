import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns
import pyspectre as ps
import pynut as nut
from rich.progress import Progress

sns.set_theme(style = "darkgrid")

netlist: str = './testbench.scs'

session = ps.start_session(netlist, [])

_   = ps.set_parameter(session, 'minc', 1e-18)

res = ps.run_all(session)['tran']

time = res.time.values

i_hi = (res['Ihi:drainin']).values
i_lo = (res['Ilo:drainin']).values
v_hi = (res.supp - res.out).values
v_lo = (res.out - res.supn).values
p_hi = i_hi * v_hi
p_lo = i_lo * v_lo

plt.plot(time, i_hi, label = 'hi')
plt.plot(time, i_lo, label = 'lo')
plt.xlabel('t in s')
plt.ylabel('I in A')
plt.legend()
plt.show()

plt.plot(time, res['ghi']-res['out'], label = 'hi')
plt.plot(time, res['glo']-res['supn'], label = 'lo')
plt.xlabel('t in s')
plt.ylabel('Vg in V')
plt.legend()
plt.show()

#_, ((ax1, ax2),(ax3,ax4)) = plt.subplots(nrows=2, ncols=2, figsize=(8, 4))
_, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, figsize=(8, 4))

_ = ax1.set_title('ghi')
_ = ax2.set_title('glo')
_ = ax1.set_xlabel('t in s')
_ = ax2.set_xlabel('t in s')
_ = ax1.set_ylabel('Vg in V')
_ = ax2.set_ylabel('Vg in V')

#_ = ax3.set_title('Ihi')
#_ = ax4.set_title('Ilo')
#_ = ax3.set_xlabel('t in s')
#_ = ax4.set_xlabel('t in s')
#_ = ax3.set_ylabel('Ihi.gate in V')
#_ = ax4.set_ylabel('Ilo.gate in V')

#def run_sim(c):
#    _  = print(f'cmin={c}e-18 C')
#    _  = ps.set_parameter(session, 'minc', c * 1e-18)
#    r  = ps.run_all(session)['tran']
#    t  = r.time.values
#    hi = r['ghi']-res['out']
#    lo = r['glo']-res['supn']
#    _  = ax1.plot(t, hi, label = f'ghi @ cmin={c}e-18 C')
#    _  = ax2.plot(t, lo, label = f'glo @ cmin={c}e-18 C')
#    ihi = r['Ihi.drain']-res['Ihi.source']
#    ilo = r['Ilo.drain']-res['Ilo.source']
#    _  = ax3.plot(t, ihi, label = f'ihi @ cmin={c}e-18 C')
#    _  = ax4.plot(t, ilo, label = f'ilo @ cmin={c}e-18 C')

def run_sim(c):
    _  = print(f'cmin={c}e-18 C')
    _  = ps.set_parameter(session, 'minc', c * 1e-18)
    r  = ps.run_all(session)['tran']
    t  = r[(r.time > 9.999e-6) & (r.time < 10.02e-6)].time
    hi = r.iloc[t.index]['Ihi:drainin']
    lo = r.iloc[t.index]['Ilo:drainin']
    _  = ax1.plot(t.values, hi, label = f'hi @ cmin={c}e-18 C')
    _  = ax2.plot(t.values, lo, label = f'lo @ cmin={c}e-18 C')

#_ = [run_sim(c) for c in list(np.linspace(1,100,10))]
_ = [run_sim(c) for c in list(np.logspace(0,2,num=10))]

plt.legend()
plt.show()

cols = ['Rgon', 'Iout', 'peak_hi', 'peak_lo']

def sim(rgon,iout):
    _       = ps.set_parameters(session, {'Rgon': rgon, 'I_L': iout})
    res     = ps.run_all(session)["Transient Analysis `tran': time = (0 s -> 17 us)"]
    i_hi    = res['I11:2'].values
    i_lo    = res['I10:2'].values
    v_hi    = (res.plus_Vq - res.Vout).values
    v_lo    = (res.Vout - res.minus_Vq).values
    peak_hi = np.max(i_hi * v_hi)
    peak_lo = np.max(i_lo * v_lo)
    return pd.DataFrame(np.array([[rgon, iout, peak_hi, peak_lo]]), columns = cols)

rgons = np.linspace(1.0,10.0,10)
iouts = np.linspace(-60,60,10)

with Progress() as progress:
    simulation = progress.add_task( 'Simulating ...'
                                  , total = len(rgons) * len(iouts) )
    def _sim(r,i):
        progress.update(simulation, advance = 1.0)
        return sim(r,i)
    df = pd.concat( [ _sim(r,i) for i in np.linspace(-60,60,10)
                                 for r in np.linspace(1.0,10.0,10)]
                  , axis = 0, ignore_index = True )

ax = plt.axes(projection ="3d")
ax.scatter3D(df.Rgon.values, df.Iout.values, df.peak_hi.values, label = 'hi')
ax.scatter3D(df.Rgon.values, df.Iout.values, df.peak_lo.values, label = 'lo')
ax.set_xlabel('$R_{g,on}$ in $\Omega$')
ax.set_ylabel('$I_{out}$ in A')
ax.set_zlabel('$P$ in W')
plt.legend()
plt.show()

# raw = pd.concat( list(nut.plot_dict(nut.read_raw('./netlist.raw')).values())[:-1]
#                , ignore_index = True
#                , axis         = 0 )