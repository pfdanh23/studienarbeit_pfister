import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import pyspectre as ps
import pynut as nut
from rich.progress import Progress

netlist: str = './netlist.scs'

session = ps.start_session(netlist, [])

res = ps.run_all(session)["Transient Analysis `tran': time = (0 s -> 17 us)"]

time = res.time
i_hi = res['I11:2'].values
i_lo = res['I10:2'].values
v_hi = (res.plus_Vq - res.Vout).values
v_lo = (res.Vout - res.minus_Vq).values
p_hi = i_hi * v_hi
p_lo = i_lo * v_lo

plt.plot(time, p_hi, label = 'hi')
plt.plot(time, p_lo, label = 'lo')
plt.xlabel('$t$ in s')
plt.ylabel('$P$ in W')
plt.legend()
plt.show()

cols = ['Rgon', 'Iout', 'peak_hi', 'peak_lo']

def sim(rgon,iout):
    _       = ps.set_parameters(session, {'Rgon': rgon, 'I_L': iout})
    res     = ps.run_all(session)["Transient Analysis `tran': time = (0 s -> 17 us)"]
    i_hi    = res['I11:2'].values
    i_lo    = res['I10:2'].values
    v_hi    = (res.plus_Vq - res.Vout).values
    v_lo    = (res.Vout - res.minus_Vq).values
    peak_hi = np.max(i_hi * v_hi)
    peak_lo = np.max(i_lo * v_lo)
    return pd.DataFrame(np.array([[rgon, iout, peak_hi, peak_lo]]), columns = cols)

rgons = np.linspace(1.0,10.0,10)
iouts = np.linspace(-60,60,10)

with Progress() as progress:
    simulation = progress.add_task( 'Simulating ...'
                                  , total = len(rgons) * len(iouts) )
    def _sim(r,i):
        progress.update(simulation, advance = 1.0)
        return sim(r,i)
    df = pd.concat( [ _sim(r,i) for i in np.linspace(-60,60,10)
                                 for r in np.linspace(1.0,10.0,10)]
                  , axis = 0, ignore_index = True )

ax = plt.axes(projection ="3d")
ax.scatter3D(df.Rgon.values, df.Iout.values, df.peak_hi.values, label = 'hi')
ax.scatter3D(df.Rgon.values, df.Iout.values, df.peak_lo.values, label = 'lo')
ax.set_xlabel('$R_{g,on}$ in $\Omega$')
ax.set_ylabel('$I_{out}$ in A')
ax.set_zlabel('$P$ in W')
plt.legend()
plt.show()

# raw = pd.concat( list(nut.plot_dict(nut.read_raw('./netlist.raw')).values())[:-1]
#                , ignore_index = True
#                , axis         = 0 )