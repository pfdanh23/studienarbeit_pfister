'''
all needed functions for project 'studienarbeit' by Daniel Pfister
'''
import pandas as pd
import numpy as np
import pyspectre as ps


def integrate_power(df:pd.DataFrame, zoom:pd.Series, epsilon:float):
    # calc time where nothing is expected to happen
    t1 = df.time.loc[zoom[0]]-2.5e-6
    t2 = df.time.loc[zoom[0]]-0.5e-6
    t3 = df.time.loc[zoom[-1]]+0.5e-6
    t4 = df.time.loc[zoom[-1]]+2.5e-6

    zoom_before = df.time[(df.time > t1) & (df.time < t2)].index
    zoom_after = df.time[(df.time > t3) & (df.time < t4)].index

    # set limit to mean*(1+epsilon) and if its lower than 1 set to 1
    p_high_lim_before = max(df['p_high'].loc[zoom_before].mean(skipna=True) * (1+epsilon), 1)
    p_low_lim_before = max(df['p_low'].loc[zoom_before].mean(skipna=True) * (1+epsilon), 1)
    p_high_lim_after = max(df['p_high'].loc[zoom_after].mean(skipna=True) * (1+epsilon), 1)
    p_low_lim_after = max(df['p_low'].loc[zoom_after].mean(skipna=True) * (1+epsilon), 1) 

    # find start/end points bevor/after the max; exceptions when no ponts are found -> set to start/end of zoom window
    try:
        start_p_high = df.p_high[df.p_high > abs(p_high_lim_before)].loc[zoom[0]:df.p_high.loc[zoom].idxmax()].index[0]
        # print('nothing to catch at start_p_high !!')
    except IndexError:
        start_p_high = df.time[zoom].index[0]
        print('caught start_p_high !!')
    try:
        end_p_high = df.p_high[df.p_high > abs(p_high_lim_after)].loc[df.p_high.loc[zoom].idxmax():zoom[-1]].index[-1]
        # print('nothing to catch at end_p_high !!')
    except IndexError:
        end_p_high = df.time[zoom].index[-1]
        print('caught end_p_high !!')
    try:    
        start_p_low = df.p_low[df.p_low > abs(p_low_lim_before)].loc[zoom[0]:df.p_low.loc[zoom].idxmax()].index[0]
        # print('nothing to catch at start_p_low !!')
    except IndexError:
        start_p_low = df.time[zoom].index[0]
        print('caught start_p_low !!')
    try:        
        end_p_low = df.p_low[df.p_low > abs(p_low_lim_after)].loc[df.p_low.loc[zoom].idxmax():zoom[-1]].index[-1]
        # print('nothing to catch at end_p_low !!')
    except IndexError:
        end_p_low = df.time[zoom].index[-1]
        print('caught end_p_low !!')

    # calc energie as integal via trapez method
    energie_high = np.trapz(df['p_high'][start_p_high:end_p_high], x=df['time'][start_p_high:end_p_high])
    energie_low = np.trapz(df['p_low'][start_p_low:end_p_low], x=df['time'][start_p_low:end_p_low])

    # save all needed values
    dict = {
        "start_p_high": start_p_high,
        "end_p_high": end_p_high,
        "start_p_low": start_p_low,
        "end_p_low": end_p_low,
        "energie_high": energie_high,
        "energie_low": energie_low
        }
    return dict

def simulate_testbench(session:ps.Session, vsup:float, iload:float, rgon:float, rgoff:float, t_dead:float):
    # set parameters to spectre session
    _   = ps.set_parameter(session, 'minc', 10e-18)
    _   = ps.set_parameter(session, 'vsup', vsup)
    _   = ps.set_parameter(session, 'iload', iload)
    _   = ps.set_parameter(session, 'rg1', rgon)
    _   = ps.set_parameter(session, 'rg0', rgoff)
    _   = ps.set_parameter(session, 'tdead', t_dead)

    # run spectre session
    return ps.run_all(session)['tran']

def get_coloumns_of_interest(res:pd.DataFrame):
    # put all needed data from simulation in dataframe
    cois = ['time', 'i_high', 'i_low', 'v_high', 'v_low', 'p_high', 'p_low']

    # rename the draincurrent
    res.rename(columns={'Ihi:drainin': 'i_high', 'Ilo:drainin': 'i_low'}, inplace=True)

    # calculate drain voltages and power
    res['v_high'] = res.supp - res.out
    res['v_low'] = res.out - res.supn
    res['p_high'] = res.i_high * res.v_high
    res['p_low'] = res.i_low * res.v_low

    # set power lower than 0 to 0
    res.p_high = res.p_high.apply(lambda x: max(x, 0))
    res.p_low = res.p_low.apply(lambda x: max(x, 0))
    return res[cois]

def calc_sw_loss(df:pd.DataFrame, vsup:float, iload:float, rgon:float, rgoff:float, t_dead:float):
    # define time space where peaks happen
    # zoom_1 = df.time[(df.time > 4.7e-6) & (df.time < 5.2e-6)].index
    zoom_1 = df.time[(df.time > 5e-6 - 3*t_dead) & (df.time < 5e-6 + 3*t_dead)].index
    # zoom_2 = df.time[(df.time > 9.7e-6) & (df.time < 10.2e-6)].index
    zoom_2 = df.time[(df.time > 10e-6 - 3*t_dead) & (df.time < 10e-6 + 3*t_dead)].index

    # calc integration start/end and the energie of power curve
    switch_1 = integrate_power(df, zoom_1, 0.1)
    switch_2 = integrate_power(df, zoom_2, 0.1)

    e_high = switch_1['energie_high'] + switch_2['energie_high']
    e_low = switch_1['energie_low'] + switch_2['energie_low']
    return_list = [vsup, iload, rgon, rgoff, t_dead, e_high, e_low]

    return np.array(return_list)

def create_new_row(data:dict, res_list:np.array):
    vsup_max = data['vsup_max']
    iload_max = data['iload_max']

    data_comp = data | {'vsup': res_list[0],      
    'vsup_ratio': res_list[0]/vsup_max,  
    'iload': res_list[1],                       
    'iload_ratio': res_list[1]/iload_max, 
    'rgon': res_list[2],                 
    'rgoff': res_list[3],                
    't_dead': res_list[4],                
    'energy_high': res_list[5],                  
    'energy_low': res_list[6]   
    }

    return pd.DataFrame.from_records({k: [v] for k,v in data_comp.items()})

def print_progress(vsup:float, i:float, rgon:float, rgoff:float, t_dead:float, progressbar=True):
    if(progressbar):
        print(f'Simulating:  Vsup = {round(vsup,2)} V;  iload = {round(i,2)} A;  rgon = {round(rgon,2)} Ohms;  rgoff = {round(rgoff,2)} Ohms;  t_dead = {round(t_dead*1e9,2)} ns')