import pandas as pd
import numpy as np
import utils
import pyspectre as ps
from matplotlib import pyplot as plt
from scipy.stats import qmc
import transistors

# function to run the simulation loop
def run(vsup:float, i:float, rgon:float, rgoff:float, t_dead:float, session:ps.Session, data:dict) -> pd.DataFrame:
    # show progress to terminal
    utils.print_progress(vsup, i, rgon, rgoff, t_dead)
    
    # simulate with given parametes
    try:
        res = utils.simulate_testbench(session, vsup, i, rgon, rgoff, t_dead)
    except:
        print('simulation failed!!')
        # del session
        # session = ps.start_session(netlist, [])
        return None

    # pick only interessted columns and calculate power
    df_coi = utils.get_coloumns_of_interest(res)

    # calculate switching loss as integral of power
    res_list = utils.calc_sw_loss(df_coi, vsup, i, rgon, rgoff, t_dead)

    # save data
    new_row = utils.create_new_row(data, res_list)

    return new_row


def run_tranistor_sim(name:str):
    # get transistor data
    data = transistors.get(name)

    # create empty df to save result
    df_to_save = pd.DataFrame()

    # start simulation session once
    netlist: str = f'./testbench_{name}.scs'
    session = ps.start_session(netlist, [])

    # define ranges for simulation
    vsup_max = data['vsup_max']
    iload_max = data['iload_max']
    iload_min = data['iload_min']
    range_vsup = [0, vsup_max]
    range_iload = [iload_min, iload_max]
    range_rgon = [1, 30]
    range_rgoff = [0.1, 3]
    range_tdead = [10e-9, 250e-9]

    vsup=200
    iload=20
    rgon = 10
    rgoff = 1
    t_dead = 100e-9
    vsup_max = 200
    iload_max = 60

    # create latin hyper cube
    n_samples = 5000
    sampler = qmc.LatinHypercube(d=5)
    lhs_sample = sampler.random(n=n_samples)
    lower_lims = np.array([range_vsup[0], range_iload[0], range_rgon[0], range_rgoff[0], range_tdead[0]])
    upper_lims = np.array([range_vsup[1], range_iload[1], range_rgon[1], range_rgoff[1], range_tdead[1]])

    # sampler = qmc.LatinHypercube(d=2)
    # lhs_sample = sampler.random(n=n_samples)
    # lower_lims = np.array([range_rgon[0], range_rgoff[0]])
    # upper_lims = np.array([range_rgon[1], range_rgoff[1]])
    lhs_sample_scaled = qmc.scale(lhs_sample, lower_lims, upper_lims)

    # simulate but save sometimes as backup
    run_list=[]
    for cnt, vals in enumerate(lhs_sample_scaled):
        # check if rgon bigger than rgoff
        if vals[2] > vals[3]:
        # if vals[0] > vals[1]:
            # simulate with vals for this iteration
            run_list.append(run(vals[0], vals[1], vals[2], vals[3], vals[4], session, data))
            # run_list.append(run(vsup, iload, vals[0], vals[1], t_dead, session, data))
            
            # save every 50-th time the result
            if (cnt % 50 == 0) & (cnt > 0):
                # add new reults to df
                df_to_save = pd.concat(run_list)
                # save longer df as backup
                df_to_save.to_csv(f'./data/{name}_temp.csv', index=False, sep=';')
                print(f'saved {cnt+1} simulation results')
                # restart session to reduce simulation time
                del session
                session = ps.start_session(netlist, [])

    # add results to df
    df_to_save = pd.concat(run_list)

    # final save of result as csv
    df_to_save.to_csv(f'./data/{name}_final.csv', index=False, sep=';')
    print(f'saved {n_samples} simulation results')

    print(df_to_save)

transistors_list = ['GS66516T', 'GS66508T', 'GS66506T', 'GS66504B', 'GS66502B']
# run_tranistor_sim('GS66516T')
for transis in transistors_list:
    run_tranistor_sim(transis)